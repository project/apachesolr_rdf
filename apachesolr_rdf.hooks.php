<?php

/**
 * @file
 * This file contains no working PHP code; it exists to provide additional
 * documentation for doxygen as well as to document hooks in the standard Drupal
 * manner.
 *
 * Example code is taken from apachesolr_rdf.module.
 * 
 * // TODO Bring this up-to-date regarding new layout.
 */

/**
 * Tell the apachesolr_rdf module about schemas for which the index and search
 * methods are implemented.
 *
 * @return
 * An associative array with schema ids as keys and associative
 * arrays with the schema data as values. The schema data consists of the
 * following items:
 * - name: A translated, human-readable name for the schema.
 * - description: A translated description of what the schema does, its
 *     specifics and what has to be done to use it.
 * - @ref create_document
 * - @ref search_form_alter
 * - @ref execute_search
 *
 * The three functions have to be specified as associative arrays with the
 * following keys:
 * - module: The module defining this function.
 * - file: The file with the function definition, relative to the module's
 *     base directory.
 * - function: The name of the function.
 */
function apachesolr_rdf_apachesolr_rdf_schemas() {
  $path = drupal_get_path('module', 'apachesolr_rdf');
  $real_path = realpath($path);
  if ($real_path) {
    $path = $real_path;
  }

  return array(
    'apachesolr_rdf_dynamic' => array(
      'name' => t('Dynamic fields'),
      'description' => t('<p>This schema uses dynamic fields to store all ' .
          'predicates related to their objects. This allows for exact ' .
          'queries using the resource URIs, and for facetting/filtering ' .
          'according to specific property values, in addition to normal ' .
          'text searches. This schema is best suited for data of a fixed ' .
          'schema, with relatively few properties that most resources will' .
          'have.</p><p>Use the "~.dynamic_fields.xml" schema and solrconfig ' .
          'files from @dir for Solr servers with this schema. You will also ' .
          'have to copy or link the ' .
          "sindice-url-preserving-tokenizer.jar file from there to Solr's lib" .
          'directory ($SOLR_HOME/example/solr/lib if using the example ' .
          'application).</p>', array('@dir' => $path)),
      'create_document' => array(
        'module' => 'apachesolr_rdf',
        'file' => 'apachesolr_rdf.index.inc',
        'function' => 'apachesolr_rdf_create_document_dynamic',
      ),
      'search_form_alter' => array(
        'module' => 'apachesolr_rdf',
        'file' => 'apachesolr_rdf.pages.inc',
        'function' => 'apachesolr_rdf_search_form_alter_dynamic',
      ),
      'execute_search' => array(
        'module' => 'apachesolr_rdf',
        'file' => 'apachesolr_rdf.pages.inc',
        'function' => 'apachesolr_rdf_execute_search_dynamic',
      ),
    ),
  );
}

/**
 * Creates an Apache_Solr_Document from the specified resource.
 *
 * @param $uri A string specifying the indexed resource's URI.
 * @param $predicates An array of predicate URIs mapped to arrays of objects
 *        that are used for that subject/predicate pair. The string
 *        value and type of these objects can be extracted with the
 *        apachesolr_rdf_extract_object_string() function.
 * @param $context A string specifying the URI of the context that indexing
 *        is done for.
 * @return An object of type Apache_Solr_Document, containing information
 *         built from the parameters.
 */
function create_document($uri, $predicates, $context) {
  $doc = new Apache_Solr_Document;

  $doc->uri_context = apachesolr_rdf_create_id($uri, $context);
  $doc->uri = $uri;
  $doc->context = $context;
  foreach ($predicates as $predicate => $objects) {
    foreach ($objects as $object) {
      $object = apachesolr_rdf_extract_object_string($object);
      $type = $object['type'];
      $string = $object['string'];
      $doc->setMultiValue('property_object', "$predicate $string");
      if ($type == 'uri') {
        $property = $predicate . '_s';
      }
      else {
        $property = $predicate . '_t';
      }
      $doc->setMultiValue($property, $string);
    }
  }

  return $doc;
}

/**
 * Alters the search form by adding advanced RDF search options and telling the
 * form to use the right submit function.
 *
 * @param $form As in hook_form_alter().
 * @param $form_state As in hook_form_alter().
 * @param $id The search id as specified in the settings.
 */
function search_form_alter(&$form, &$form_state, $id) {
  $form['#submit'] = array('apachesolr_rdf_search_submit');

  $advanced = array(
    '#type' => 'fieldset',
    '#title' => t('Advanced Search'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $advanced['context'] = array(
    '#type' => 'textfield',
    '#title' => t('Context'),
    '#autocomplete_path' => "apachesolr_rdf/context/autocomplete/$id",
  );

  $advanced['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Search'),
  );

  $form['advanced'] = $advanced;
}

/**
 * Executes an Apache Solr RDF search.
 *
 * @param $options The context options as specified in the settings.
 * @param $keys The search keys.
 * @param $additional All additional GET parameters of the search.
 * @return A themed string representing the search result.
 */
function execute_search($options, $keys, $additional) {
  // TODO Paste example
}
