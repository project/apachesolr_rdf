
This module integrates the Resource Description Framework (RDF) module with
Apache Solr search servers, providing the possibility to do fulltext searches,
including faceting, on arbitrary RDF data from that module.


Note:
-----
The classes in sindice-url-preserving-tokenizer.jar were derived from the
Lucene project (http://lucene.apache.org/) and written by Renaud Debru
(http://groups.drupal.org/user/35034).
